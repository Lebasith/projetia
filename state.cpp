#include "state.h"
#include <iostream>
#include <array>
#include <random>
#include <algorithm>
#include <iterator>
#include <vector>

int findEmptyTilePosition(Board p)
{
    int emptyTilePosition = 0;
    for(int i = 0; i < p.size(); i++)
    {
        if(p[i] == 8)
        {
            emptyTilePosition = i;
        }
    }
    return emptyTilePosition;
}

PossibleMove getPossibleMoves(int position)
{
    switch(position)
    {
    case 0:
        return PossibleMove::SOUTH | PossibleMove::EAST;
    case 1:
        return PossibleMove::WEST | PossibleMove::SOUTH | PossibleMove::EAST;
    case 2:
        return PossibleMove::WEST | PossibleMove::SOUTH;
    case 3:
        return PossibleMove::NORTH | PossibleMove::EAST | PossibleMove::SOUTH;
    case 4:
        return PossibleMove::NORTH | PossibleMove ::SOUTH | PossibleMove::WEST | PossibleMove::EAST;
    case 5:
        return PossibleMove::WEST | PossibleMove::NORTH | PossibleMove::SOUTH;
    case 6:
        return PossibleMove::NORTH | PossibleMove::EAST;
    case 7:
        return PossibleMove::WEST | PossibleMove::NORTH | PossibleMove::EAST;
    case 8:
        return PossibleMove::NORTH | PossibleMove::WEST;
    }
}

bool isPossible(Board board)
{
    int compteur = 0;

    int emptyTilePos = findEmptyTilePosition(board);

    int distance = 0;
    if(emptyTilePos == 0)
    {
        distance = 4;
    }
    else if(emptyTilePos == 1 || emptyTilePos == 3)
    {
        distance = 3;
    }
    else if(emptyTilePos == 2 || emptyTilePos == 4 || emptyTilePos == 6)
    {
        distance = 2;
    }
    else if(emptyTilePos == 5 || emptyTilePos == 7)
    {
        distance = 1;
    }
    else if(emptyTilePos == 8)
    {
        distance = 0;
    }
    else
    {
        std::cout << "Bug" << std::endl;
        return false;
    }

    // tant que la board n'est pas triée
    while(!std::is_sorted(board.begin(), board.end()))
    {
        for(int i = 0; i < board.size(); i++)
        {
            // si une case est mal placée
            if(board[i] != i)
            {
                // on stocke la valeur de la case
                int tmp = board[i];
                // on effectue la permutation
                board[i] = board[tmp];
                board[tmp] = tmp;


                compteur++;
                continue;
            }            
        }

    }

    // si la parité est la même la board est possible, sinon non
    return compteur%2 == distance%2 ? true : false;
}

int getNewPosition(int position, Move d)
{
    switch (position) {
    case 0:
    if(d == Move::SOUTH)
    {
        return 3;
    }
    if(d == Move::EAST)
    {
        return 1;
    }
    break;
    case 1:
    if(d == Move::SOUTH)
    {
        return 4;
    }
    if(d == Move::EAST)
    {
        return 2;
    }
    if(d == Move::WEST)
    {
        return 0;
    }
    break;
    case 2:
    if(d == Move::SOUTH)
    {
        return 5;
    }
    if(d == Move::WEST)
    {
        return 1;
    }
    break;
    case 3:
    if(d == Move::SOUTH)
    {
        return 6;
    }
    if(d == Move::EAST)
    {
        return 4;
    }
    if(d == Move::NORTH)
    {
        return 0;
    }
    break;
    case 4:
    if(d == Move::SOUTH)
    {
        return 7;
    }
    if(d == Move::EAST)
    {
        return 5;
    }
    if(d == Move::WEST)
    {
        return 3;
    }
    if(d == Move::NORTH)
    {
        return 1;
    }
    break;
    case 5:
    if(d == Move::SOUTH)
    {
        return 8;
    }
    if(d == Move::NORTH)
    {
        return 2;
    }
    if(d == Move::WEST)
    {
        return 4;
    }
    break;
    case 6:
    if(d == Move::NORTH)
    {
        return 3;
    }
    if(d == Move::EAST)
    {
        return 7;
    }
    break;
    case 7:
    if(d == Move::NORTH)
    {
        return 4;
    }
    if(d == Move::EAST)
    {
        return 8;
    }
    if(d == Move::WEST)
    {
        return 6;
    }
    break;
    case 8:
    if(d == Move::NORTH)
    {
        return 5;
    }
    if(d == Move::WEST)
    {
        return 7;
    }
    break;
    }

    std::cout << "Can't find new position" << std::endl;
}

size_t State::generateHash()
{
    int i = 0;
    for(int e : m_board)
    {
        i *= 10;
        i += e;
    }
    std::hash<int> hashFunction;
    size_t hash = hashFunction(i);

    return hash;
}

State::State(Board b, Move d)
    :m_lastMove(d)
{
    int emptyTilePosition = findEmptyTilePosition(b);
    int newPos = getNewPosition(emptyTilePosition, d);
    Board newBoard = Board(b);

    newBoard.at(emptyTilePosition) = b.at(newPos);
    newBoard.at(newPos) = b.at(emptyTilePosition);

    m_emptyTilePosition = newPos;
    m_board = newBoard;
    m_possibleMoves = getPossibleMoves(m_emptyTilePosition);
    m_hash = generateHash();   
}

FinalState::FinalState()
{
    m_board = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
    m_emptyTilePosition = findEmptyTilePosition(m_board);
    m_possibleMoves = getPossibleMoves(m_emptyTilePosition);
    m_hash = generateHash();
    m_lastMove = Move::NONE;
}

float State::computeHeuristics() const
{
    Board distances;
    Board heuristics;

    for(int i = 0 ; i < 9; i++)
    {
        distances[i] = std::abs(m_board[i] / 3 - i / 3) + std::abs(m_board[i] % 3 - i % 3);
    }



/*
    for(int i = 0; i < m_board.size(); i++)
    {
         std::cout << "Distance au point :" << i << " : " << distances[i] << std::endl;
    }
*/

    std::array<std::array<int, 9>, 6> weights;

    weights[0] = {36, 12, 12, 4, 1, 1, 4, 1, 0};
    weights[1] = {8, 7, 6, 5, 4, 3, 2, 1, 0};
    weights[2] = {8, 7, 6, 5, 3, 2, 4, 1, 0};
    weights[3] = {1, 1, 1, 1, 1, 1, 1, 1, 0};
    weights[4] = {128, 64, 32, 16, 8, 4, 2, 1,0};
    weights[5] = {128, 64, 32, 16, 8, 16, 32, 64, 128};

    float h = 0;

    for(int i = 0; i < 9; i++)
    {
        h += weights[1][i] * distances[i] * 4;
    }

    return h;
}

State& compareHeuristics(State& a, State& b)
{
    return a.computeHeuristics() > b.computeHeuristics() ? a : b;
}

std::vector<State> State::getBestReachableStates()
{
    std::vector<State> reachableStates = getReachableStates();

    std::set<State, StateCompare> stateSet = std::set<State, StateCompare>();

    for(State states : reachableStates)
    {
        stateSet.insert(states);
    }

    reachableStates = std::vector<State>(stateSet.begin(), stateSet.end());

    return reachableStates;
}

Board generateRandomBoard()
{
    Board p = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };

    std::random_device randomDevice;
    std::mt19937 randomGenerator(randomDevice());

    std::shuffle(p.begin(), p.end(), randomGenerator);

    while(!isPossible(p))
    {
        p = generateRandomBoard();
    }

    return p;
}

BaseState::BaseState()
{
    m_board = generateRandomBoard();
    m_emptyTilePosition = findEmptyTilePosition(m_board);
    m_possibleMoves = getPossibleMoves(m_emptyTilePosition);
    m_hash = generateHash();
    m_lastMove = Move::NONE;
}

BaseState::BaseState(Board b)
{
    if(!isPossible(b))
    {
        std::cout << "Ce plateau n'est pas possible" << std::endl;
        exit(0);
    }
    else
    {
        m_board = b;
        m_emptyTilePosition = findEmptyTilePosition(m_board);
        m_possibleMoves = getPossibleMoves(m_emptyTilePosition);
        m_hash = generateHash();
        m_lastMove = Move::NONE;
    }
}

IntermediateState::IntermediateState(BaseState* baseState, std::vector<Move> previousMoves, Move m)
    :m_baseState(baseState),
      m_movesFromBaseState(previousMoves)
{
    m_movesFromBaseState.push_back(m);

    m_board = baseState->board();

    for(Move m : m_movesFromBaseState)
    {
        int emptyTilePosition = findEmptyTilePosition(m_board);
        int newPos = getNewPosition(emptyTilePosition, m);
        Board newBoard = Board(m_board);

        newBoard.at(emptyTilePosition) = m_board.at(newPos);
        newBoard.at(newPos) = m_board.at(emptyTilePosition);

        m_emptyTilePosition = newPos;
        m_board = newBoard;
    }

    m_possibleMoves = getPossibleMoves(m_emptyTilePosition);
    m_hash = generateHash();
}


void State::description()
{

    std::cout << "Hash :" << m_hash << std::endl;
    displayBoard();
    std::cout << "Position de la tuile vide : " << m_emptyTilePosition << std::endl;
    std::cout << "Deplacements possibles : ";
    if(m_possibleMoves & PossibleMove::NORTH)
        std::cout << "Nord ";
    if(m_possibleMoves & PossibleMove::SOUTH)
        std::cout << "Sud ";
    if(m_possibleMoves & PossibleMove::EAST)
        std::cout << "Est ";
    if(m_possibleMoves & PossibleMove::WEST)
        std::cout << "Ouest ";
    std::cout << std::endl;
}

void IntermediateState::description()
{
    std::cout << "Hash :" << m_hash << " , F : " << getF() << std::endl;
    displayBoard();
//    std::cout << "Board Possible : " << isPossible(m_board) << std::endl;
    std::cout << "Position de la tuile vide : " << m_emptyTilePosition;
    std::cout << ", Deplacements possibles : ";
    if(m_possibleMoves & PossibleMove::NORTH)
        std::cout << "Nord ";
    if(m_possibleMoves & PossibleMove::SOUTH)
        std::cout << "Sud ";
    if(m_possibleMoves & PossibleMove::EAST)
        std::cout << "Est ";
    if(m_possibleMoves & PossibleMove::WEST)
        std::cout << "Ouest ";
    std::cout << std::endl;

}

void State::displayBoard()
{
    std::cout << "Board : " << std::endl;
    /*
    std::cout << m_board[0] << m_board[1] << m_board[2] << std::endl;
    std::cout << m_board[3] << m_board[4] << m_board[5] << std::endl;
    std::cout << m_board[6] << m_board[7] << m_board[8] << std::endl;
    */

    for(int i = 0; i < 9; i++)
    {
        if(m_board[i] == 8)
            std::cout << "_";
        else std::cout << m_board[i];

        if((i+1)%3 == 0)
            std::cout << std::endl;
    }

}

std::vector<State> State::getReachableStates()
{
    std::vector<State> reachableStates = std::vector<State>();

    if(m_possibleMoves & PossibleMove::NORTH)
    {
        State newState = State(m_board, Move::NORTH);
        reachableStates.push_back(newState);
    }
    if(m_possibleMoves & PossibleMove::SOUTH)
    {
        State newState = State(m_board, Move::SOUTH);
        reachableStates.push_back(newState);
    }
    if(m_possibleMoves & PossibleMove::EAST)
    {
        State newState = State(m_board, Move::EAST);
        reachableStates.push_back(newState);
    }
    if(m_possibleMoves & PossibleMove::WEST)
    {
        State newState = State(m_board, Move::WEST);
        reachableStates.push_back(newState);
    }

    return reachableStates;
}

std::vector<Move> State::getMoves()
{
    std::vector<Move> moves = std::vector<Move>();

    if(m_possibleMoves & PossibleMove::NORTH)
    {
        moves.push_back(Move::NORTH);
    }
    if(m_possibleMoves & PossibleMove::SOUTH)
    {
        moves.push_back(Move::SOUTH);
    }
    if(m_possibleMoves & PossibleMove::EAST)
    {
        moves.push_back(Move::EAST);
    }
    if(m_possibleMoves & PossibleMove::WEST)
    {
        moves.push_back(Move::WEST);
    }

    return moves;
}
