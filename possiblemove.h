#ifndef DEPLACEMENT_H
#define DEPLACEMENT_H

#include <cstdint>

enum class PossibleMove : std::uint8_t
{
    NORTH       = 1 << 0,
    SOUTH       = 1 << 1,
    EAST        = 1 << 2,
    WEST        = 1 << 3,
    NONE        = 1 << 4
};

inline PossibleMove operator ++(PossibleMove& d, int)
{
    switch (d) {
    case PossibleMove::NORTH:
        return d = PossibleMove::SOUTH;
        break;
    case PossibleMove::SOUTH:
        return d = PossibleMove::EAST;
        break;
    case PossibleMove::EAST:
        return d = PossibleMove::WEST;
        break;
    case PossibleMove::WEST:
        return d = PossibleMove::NONE;
        break;
    case PossibleMove::NONE:
        return d = PossibleMove::NORTH;
        break;
    }
}

// activer un flag
inline PossibleMove operator | (PossibleMove a, PossibleMove b)
{
    return static_cast<PossibleMove>(static_cast<std::uint8_t>(a) | static_cast<std::uint8_t>(b));
}

// tester un flagq
inline bool operator&(PossibleMove a, PossibleMove b)
{
    return static_cast<std::uint8_t>(a) & static_cast<std::uint8_t>(b);
}

enum class Move
{
    NORTH, SOUTH, EAST, WEST, NONE
};

#endif // DEPLACEMENT_H

