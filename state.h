#ifndef ETAT_H
#define ETAT_H

#include <array>
#include <vector>
#include "possiblemove.h"
#include <set>

using Board = std::array<int, 9>;


class State
{   

public:
    State(Board b, Move d);

    std::vector<State> getReachableStates();
    std::vector<State> getBestReachableStates();
    std::vector<Move> getMoves();
    size_t generateHash();
    float computeHeuristics() const;

    inline int emptyTilePosition() const
    {
        return m_emptyTilePosition;
    }

    inline Board board() const
    {
        return m_board;
    }

    inline size_t hash() const
    {
        return m_hash;
    }

    inline Move lastMove() const
    {
        return m_lastMove;
    }

    virtual void description();
    void displayBoard();

protected:
    State() { }
    Board m_board;
    Board m_elementaryDistances;
    int m_emptyTilePosition;
    PossibleMove m_possibleMoves;
    size_t m_hash;
    Move m_lastMove;
};

struct StateCompare{
    bool operator()(State const &a, State const &b)
    {
        return a.computeHeuristics() < b.computeHeuristics() ? true : false;
    }
};



class FinalState : public State
{
public:
    FinalState();
};

class BaseState : public State
{
public:
    BaseState();
    BaseState(Board b);
};

class IntermediateState : public State
{
public :
    IntermediateState() {}
    IntermediateState(BaseState* baseState, std::vector<Move> previousMoves, Move m);

    void description() override;

    inline BaseState* baseState()
    {
        return m_baseState;
    }

    inline std::vector<Move> movesFromBaseState() const
    {
        return m_movesFromBaseState;
    }

    inline float getF() const
    {
        return computeHeuristics() + m_movesFromBaseState.size();
    }

private:
    BaseState* m_baseState;
    std::vector<Move> m_movesFromBaseState;
};

struct IntermediateStateCompare{
    bool operator()(IntermediateState const &a, IntermediateState const &b)
    {
        return a.getF() > b.getF() ? true : false;
    }
};


#endif // ETAT_H
