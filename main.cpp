#include <iostream>
#include <array>
#include <random>
#include <algorithm>
#include <iterator>
#include <vector>
#include "state.h"
#include <unordered_set>
#include "possiblemove.h"
#include <queue>
#include <chrono>
#include <ratio>
#include <ctime>

void processState(std::priority_queue<IntermediateState, std::vector<IntermediateState>, IntermediateStateCompare>& queue, IntermediateState s)
{
    std::vector<State> reachableStates = s.getReachableStates();
    for(int i = 0; i < reachableStates.size(); i++)
    {
        IntermediateState nextState = IntermediateState(s.baseState(), s.movesFromBaseState(), reachableStates[i].lastMove());
        //std::cout << "Etat ajouté, profondeur : " << nextState.movesFromBaseState().size() << std::endl;
        //nextState.description();
        queue.push(nextState);
    }
}

int main(int argc, char *argv[])
{
    std::cout << "Jeu du Taquin !" << std::endl;
    // Début du timer
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

    // Création d'un état initial "viable
//    BaseState baseState = BaseState();
//    BaseState baseState = BaseState({6, 1, 5, 0, 2, 7, 8, 4, 3});
    BaseState baseState = BaseState({1, 0, 5, 3, 7, 6, 8, 2, 4});
    std::cout << "Etat initial : " << std::endl;
    baseState.description();

    // Création de l'état à atteindre
    FinalState finalState = FinalState();

    // Hashtable pour stocker les états déjà visités
    std::unordered_set<int> hashTable = std::unordered_set<int>();
    hashTable.insert(baseState.hash());

    // Pile pour stocker les états à visiter, stockée par ordre décroissant d'heuristique
    std::priority_queue<IntermediateState, std::vector<IntermediateState>, IntermediateStateCompare> queue;


    // Récupère les états atteignables
    std::vector<State> reachableStates = baseState.getReachableStates();
    // Pour chacun d'eux
    for(int i = 0; i < reachableStates.size(); i++)
    {
        std::vector<Move> movesDone = std::vector<Move>();
        // Création de l'état atteignable
        IntermediateState nextState = IntermediateState(&baseState, movesDone, reachableStates[i].lastMove());
        // Ajout des états atteignables à la pile
        queue.push(nextState);
    }
    int count = 0;

    IntermediateState foundState;

    for(;;)
    {
        // On prend le premier état de la pile
        IntermediateState s = queue.top();

        // si c'est l'état final
        if(s.hash() == finalState.hash())
        {
            std::cout << "Etat Initial : " << baseState.hash() << std::endl;
            baseState.displayBoard();
            std::cout << "Etat trouvé : " << std::endl;
            s.description();
            foundState = s;
            break;
        }

        // On retire cet état de la pile
        queue.pop();

        std::unordered_set<int>::const_iterator it = hashTable.find(s.hash());
        // Si cet état n'a pas été visité

        if(it == hashTable.end())
        {
            hashTable.insert(s.hash());
            // On le visite (ajout des états atteignables dans la pile)
            processState(queue, s);
            //std::cout << "Etat visité" << std::endl;
            //s.description();
        }
        else
        {
            count++;
        }


    }

    // On affiche les mouvements effectués depuis l'état initial
    std::cout << "Moves : ";
    for(Move m : foundState.movesFromBaseState())
    {
        std::string move;
        switch (m) {
        case Move::NORTH:
            move = "NORTH, ";
            break;
        case Move::SOUTH:
            move = "SOUTH, ";
            break;
        case Move::EAST:
            move = "EAST, ";
            break;
        case Move::WEST:
            move = "WEST, ";
            break;
        default:
            break;
        }
        std::cout << move;
    }
    std::cout << std::endl;
    std::cout << "Nombre de movements : " << foundState.movesFromBaseState().size() << std::endl;
    std::cout << "Taille de la hashtable : " << hashTable.size() << std::endl;
    std::cout << "Nombre d'états retirés de la file : " << count << std::endl;

    // Fin du timer
    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
    // Calcul de la durée
    std::chrono::duration<double> duration = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
    std::cout << "Temps de calcul : " << duration.count() << " secondes." << std::endl;

    /*
    // Affiche les etats 1 par 1
    std::vector<Move> movesDone = std::vector<Move>();
    for(int i = 0; i < foundState.movesFromBaseState().size(); i++)
    {
        Move move = foundState.movesFromBaseState()[i];
        IntermediateState s = IntermediateState(&baseState, movesDone, move);
        movesDone.push_back(move);
        s.displayBoard();
    }
    */
    std::cout << "Fin" << std::endl;

    return 0;
}
